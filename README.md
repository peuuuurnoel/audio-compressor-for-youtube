# Audio Compressor for Youtube

Add a basic audio compressor to Youtube player.

This is a draft version which needs improvements.

Feel free to fork and customize.

# Installation/First use

Load the [userscript](https://gitlab.com/peuuuurnoel/audio-compressor-for-youtube/-/raw/master/userscript.user.js) with the recommended __Tampermonkey__ browser extension :
* [For Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
* [For Firefox](https://addons.mozilla.org/fr/firefox/addon/tampermonkey/)

You can use [Greasemonkey](https://addons.mozilla.org/fr/firefox/addon/greasemonkey/) on Firefox as well.

# Usage

Click on the button "On/Off" at the player's bottom toolbar to enable/disable the compressor.