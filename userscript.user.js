// ==UserScript==
// @name         Audio compressor for Youtube
// @namespace    AudioCompressor-YT
// @version      0.1.0-ALPHA
// @description  Add an audio compressor to youtube player
// @homepage     https://gitlab.com/peuuuurnoel/audio-compressor-for-youtube
// @downloadURL  https://gitlab.com/peuuuurnoel/audio-compressor-for-youtube/-/raw/master/userscript.user.js
// @updateURL    https://gitlab.com/peuuuurnoel/audio-compressor-for-youtube/-/raw/master/userscript.meta.js
// @author       Peuuuur Noel
// @icon         https://www.google.com/s2/favicons?domain=youtube.com
// @match        https://*.youtube.com/*
// @compatible   firefox >=53
// @compatible   chrome >=54
// ==/UserScript==

(function () {
    'use strict';

    class AudioCompressor {
        constructor() {
            console.log('[AudioCompressor] init');

            let prev = null;
            let interval = setInterval(() => {
                if (location.href !== prev) {
                    prev = location.href;
                    if (location.pathname === '/watch') {
                        console.log('[AudioCompressor] start loading');
                        clearInterval(interval);

                        let player = document.querySelector('#ytd-player');

                        let compressor = {
                            compressed: false,
                            player: player,
                            video: player.querySelector('video')
                        };
                        AudioCompressor.create(compressor);
                        AudioCompressor.addButton(compressor);
                    }
                }
            }, 1000);
        }
        static create(cpsr) {
            console.log('[AudioCompressor] create');

            cpsr.audioCtx = new AudioContext();
            cpsr.source = cpsr.audioCtx.createMediaElementSource(cpsr.video);
            cpsr.compressor = cpsr.audioCtx.createDynamicsCompressor();
            cpsr.source.connect(cpsr.audioCtx.destination);
            AudioCompressor.update(cpsr.compressor);
        }
        static addButton(cpsr) {
            console.log('[AudioCompressor] addButton');

            let button = cpsr.button = document.createElement('button');
            cpsr.player.querySelector('.ytp-right-controls').prepend(button);

            button.className = 'ytp-compressor-button ytp-button';
            button.title = 'Compressor';
            button.innerText = 'OFF';
            button.setAttribute('aria-pressed', false);
            button.style.float = 'left';
            button.style.textAlign = 'center';
            button.addEventListener("click", () => {
                AudioCompressor.toggle(cpsr);
            });
        }
        static toggle(cpsr) {
            if (!cpsr.compressed)
                AudioCompressor.enable(cpsr);
            else
                AudioCompressor.disable(cpsr);
            cpsr.compressed = !cpsr.compressed;
        }
        static enable(cpsr) {
            console.log('[AudioCompressor] enable');
            cpsr.source.disconnect(cpsr.audioCtx.destination);
            cpsr.source.connect(cpsr.compressor);
            cpsr.compressor.connect(cpsr.audioCtx.destination);
            cpsr.button.innerText = 'ON';
            cpsr.button.setAttribute('aria-pressed', true);
        }
        static disable(cpsr) {
            console.log('[AudioCompressor] disable');
            cpsr.source.disconnect(cpsr.compressor);
            cpsr.compressor.disconnect(cpsr.audioCtx.destination);
            cpsr.source.connect(cpsr.audioCtx.destination);
            cpsr.button.innerText = 'OFF';
            cpsr.button.setAttribute('aria-pressed', false);
        }
        static update(compressor) {
            console.log('[AudioCompressor] update');

            compressor.threshold.value = -50;
            compressor.knee.value = 40;
            compressor.ratio.value = 12;
            compressor.attack.value = 0;
            compressor.release.value = .25;
        }
    };

    console.log('[AudioCompressor]');
    new AudioCompressor();
})();
