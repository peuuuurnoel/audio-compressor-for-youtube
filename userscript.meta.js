// ==UserScript==
// @name         Audio compressor for Youtube
// @namespace    AudioCompressor-YT
// @version      0.1.0-ALPHA
// @description  Add an audio compressor to youtube player
// @homepage     https://gitlab.com/peuuuurnoel/audio-compressor-for-youtube
// @downloadURL  https://gitlab.com/peuuuurnoel/audio-compressor-for-youtube/-/raw/master/userscript.user.js
// @updateURL    https://gitlab.com/peuuuurnoel/audio-compressor-for-youtube/-/raw/master/userscript.meta.js
// @author       Peuuuur Noel
// @icon         https://www.google.com/s2/favicons?domain=youtube.com
// @match        https://*.youtube.com/*
// @compatible   firefox >=53
// @compatible   chrome >=54
// ==/UserScript==